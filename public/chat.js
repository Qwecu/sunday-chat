
window.onload = function() {

    var myChatroomData = [];
    var myChannels = [];
    var myChannelsAndPrivateChatsWithUnreadMessages = [];

    var socket = io.connect(window.location.href);

    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var leaveChannelButton = document.getElementById("leaveChannel");
    var usernameField = document.getElementById("usernameField");
    var usernameButton = document.getElementById("username");
    var chatArea = document.getElementById("chatArea");
    var usersBox = document.getElementById("usersBox");
    var myChannelsButtons = document.getElementById("myChannelsButtons");
    var channels = document.getElementById("channels");
    var addChannelField = document.getElementById("addChannelField");
    var addChannelButton = document.getElementById("addChannelButton"); 
    var currentChannelName = document.getElementById('currentChannelName');
    var currentChannel;
    var myName = document.getElementById("myName");
    var stickersInfo = document.getElementById('stickers');

    //a matching stickername.png has to be added to the directory
    var stickers = ['balloons', 'box', 'dog', 'rings', 'rose', 'cake', 'heart'];

    socket.on('user message', function (data) {
        if(data.message) {
            myChatroomData.push(data);
            tryAddToUnread(myChannelsAndPrivateChatsWithUnreadMessages, currentChannel, data.channel);
        } else {
            console.log("There is a problem:", data);
        }
    });

    socket.on('username approved', function(data) {
         myName.textContent = 'My name: ' + data.message;
    });

    socket.on('username change', function(data){
        var oldName = data.oldName;
        var newName = data.newName;
        if (currentChannel === oldName){
            currentChannel = newName;
        }
        for (i = 0; i < myChatroomData.length; i++) {
            var message = myChatroomData[i];
            if (message.channel === oldName) {
                message.channel = newName;
            }
        }
        var index = myChannelsAndPrivateChatsWithUnreadMessages.indexOf(oldname);
        if (index != -1){
            myChannelsAndPrivateChatsWithUnreadMessages.splice(index, 1);
            myChannelsAndPrivateChatsWithUnreadMessages.push(newName);
        }
    })

    socket.on('update client', function(data) {
        if (data.userNames) {
            drawUserNames(usersBox, data.userNames);            
        }
        if (data.channelNames) {
            drawChannelNames(channels, data.channelNames, myChannels)         
        }
        updateStickerInfo(stickersInfo, stickers);
        updateCurrentChannelName(currentChannelName);
        drawMessages(chatArea, stickers);
    });

    socket.on('announcement', function(data) {
        if(data.message) {
            myChatroomData.push(data);
            drawMessages(chatArea, stickers);
        } else {
            console.log("There is a problem:", data);
        }
    });

     socket.on('channel change accepted', function(data) {
        if (myChannels.indexOf(data.channelName) == -1) {
            myChannels.push(data.channelName);
        }
        changeChannel(data.channelName);
    });

    socket.on('leaving channel approved', function(data) {
        if (myChannels.indexOf(data.channelName) == -1) {
            return;
        }
        myChannels.splice(myChannels.indexOf(data.channelName), 1);
        deleteChannelMessages(myChatroomData, data.channelName);
        if (myChannels.length > 0) {
            changeChannel(myChannels[0]);
        } else {
            currentChannel = '';
            chatArea.innerHTML = '<h2>Join rooms to chat :)</h>';
        }
    });

    function drawUserNames(usersBox, userNames) {
        while (usersBox.hasChildNodes()){
            usersBox.removeChild(usersBox.lastChild);
        }
        for (i =0; i< userNames.length; i++){
            var button = document.createElement('BUTTON');
            button.className = 'rightsideButton';
            button.textContent = userNames[i];
            if (myChannelsAndPrivateChatsWithUnreadMessages.indexOf(userNames[i]) != -1) {
                button.className = 'rightsideButtonDarker';
            }
            button.onclick = function(){
                changeChannel(this.textContent);
                this.className = 'rightsideButton';
                field.focus();
            }
            usersBox.appendChild(button);
        }
    }

    function drawChannelNames(channels, channelNames, myChannels) {
        while (myChannelsButtons.hasChildNodes()) {
            myChannelsButtons.removeChild(myChannelsButtons.lastChild);
        }
        while (channels.hasChildNodes()) {
            channels.removeChild(channels.lastChild);
        }
        for (i =0; i<channelNames.length; i++) {
            var channelName = channelNames[i];
            var button = document.createElement('BUTTON');
            button.textContent = channelName;
            button.className = 'channelButton';
            if (myChannelsAndPrivateChatsWithUnreadMessages.indexOf(channelName) != -1) {
                button.className = 'channelButtonDarker';
            }
            var isOneOfMyChannels = (myChannels.indexOf(channelName) != -1);
            if (isOneOfMyChannels) {
                myChannelsButtons.appendChild(button);
            } else {
                channels.appendChild(button);
            }            
            button.onclick = function() {
                tryJoinChannel(this.textContent);
            }
        }
        if (!channels.hasChildNodes()) {
            var allJoined = document.createElement('H4');
            allJoined.innerText = 'You have joined them all!'
            channels.appendChild(allJoined);
        }
    }

    function deleteChannelMessages(messages, channelName) {
        var messagesToDelete = [];
        for ( i = 0; i < messages.length; i++) {
            var message = messages[i];
            if (message.channel === channelName) {
                messagesToDelete.push(message);
            }            
        }
        for (i = 0; i < messagesToDelete.length; i++) {
                messages.splice(messages.indexOf(message), 1);
        }
    }

    function tryJoinChannel(channelName) {
        socket.emit('channel join request', { channelName: channelName });
    }

    sendButton.onclick = function() {
        var text = field.value;
        field.value = '';
        if (text === ''){
            return;
        }
        socket.emit('send', { message: text , channel: currentChannel });
    };

    leaveChannelButton.onclick = function() {
        var channelName = currentChannel;
        if (myChannels.indexOf(channelName) != -1) {
            socket.emit('leave channel request', { channelName: channelName });
        } else {
            // delete messages from a private discussion
            deleteChannelMessages(myChatroomData, channelName);
            if (myChannels.length > 0) {
            changeChannel(myChannels[0]);
             } else {
                 currentChannel = '';
                 updateCurrentChannelName(currentChannelName);
                 drawMessages(chatArea, stickers);
             }
        }        
    }

    usernameButton.onclick = function() {
        var text = usernameField.value;
        usernameField.value = '';
        if (text === ''){
            return;
        }
        socket.emit('newUsernameRequest', { message: text });
    };

    addChannelButton.onclick = function() {
        var text = addChannelField.value;
        addChannelField.value = '';
        if (text === ''){
            return;
        }
        socket.emit('new channel request',{ message: text });
    }

    function changeChannel(channelName) {
        currentChannel = channelName;
        updateCurrentChannelName(currentChannelName);
        removeFromUnread(myChannelsAndPrivateChatsWithUnreadMessages, channelName);
        drawMessages(chatArea, stickers);
    }

    function tryAddToUnread(unreadMessages, currentChannel, channelName) {
            if (unreadMessages.indexOf(channelName) == -1 && channelName !== currentChannel) {
            unreadMessages.push(channelName);
        }
    }

    function removeFromUnread(unread, channel) {
        var index = unread.indexOf(channel);
        if (index != -1){
            unread.splice(index, 1);
        }
    }

    function drawMessages(container, stickers) {
        var html = '';
        if (currentChannel === ''){
            container.innerHTML = '<h2>Join rooms to chat :)</h>';
            return;
        }
        for(var i = 0; i < myChatroomData.length; i++) {
            var message = myChatroomData[i];
            if (message.channel && message.channel !== currentChannel) {
                continue;
            }
            if (!message.channel){
                message.channel = currentChannel;
            }
            if (message.senderUserName) {
                html += message.senderUserName + ': ';
            }
            var text = message.message;
            while (text.includes('<')){
                text = text.replace('<', '&lt;');
            }
            while (text.includes('>')){
                text = text.replace('>', '&gt;');
            }
            if (!message.senderUserName){
                text = '<i>' + text + '</i>';
            }
            if (stickers.indexOf(text)!= -1){
                html += '<IMG SRC="' + text + '.png" ALT="sticker" HEIGHT=50>';
            }
            else {
                html += text;
            }       
            html += '<br />';
        }        
        container.innerHTML = html;
        container.scrollTop = container.scrollHeight;
    }

    function isSticker(message, stickers) {
        for(i = 0; i < stickers.length; i++) {
            if (stickers[i] === message) {
                return true;
            }
        }
        return false;
    }
    
    function updateCurrentChannelName(currentChannelName) {
        if (myChannels.indexOf(currentChannel) == -1) {
            currentChannelName.textContent = 'private chat with ' + currentChannel;
        } else {
            currentChannelName.textContent = currentChannel;
        }
    }

    function updateStickerInfo(stickersInfo, stickers) {
        var text = 'Available stickers: ';
        for(i = 0; i < stickers.length; i++) {
            text += stickers[i];
            if (i < stickers.length - 1 ) {
                text += ', ';
            }
        }
        text += (' (type into the chat)');
        stickersInfo.textContent = text;
    }
}
