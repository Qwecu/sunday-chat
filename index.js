
var express = require('express');
var app = express();
var io = require('socket.io').listen(app.listen(48915));

connections = [];
channels = [{name: 'lobby', users: []}, {name: 'lounge', users: []}, {name: 'pool', users: []}, {name: 'backyard of the castle', users: []},
{name: 'the terrible, terrible future', users: []}, {name: 'featured channel 666', users: []}, {name: "children's corner", users: []}];

//set view engine, where template files are, which ones to use
app.set('views', __dirname + '/public');
app.set('view engine', "pug");
app.engine('pug', require('pug').__express);
app.get("/", function(req, res) {
    res.render('chatPage');
});

app.use(express.static(__dirname + '/public'));

io.sockets.on('connection', function (socket) {
    connections.push(socket);
    console.log('new connection, %s in total', connections.length);

    giveDefaultUsername(socket);
    socket.emit('username approved', { message: socket.username});

    var lobby = getChannelByName(channels, 'lobby');
    lobby.users.push(socket);

    socket.emit('channel change accepted', {channelName: 'lobby'});
    socket.emit('announcement', { message: 'welcome to SundayChat, ' + socket.username});
    io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});

    socket.on('send', function (data) {
        var channel = getChannelByName(channels, data.channel);
        if (channel != null && typeof(data.message === 'string') && data.message.length < 20000) {
            data.senderUserName = socket.username;
            var receivers = channel.users;
            for(i = 0; i < receivers.length; i++){
                receivers[i].emit('user message', data);                
            }
            io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
        } else if (channel == null && typeof(data.message === 'string') && data.message.length < 20000) {
            data.senderUserName = socket.username;
            //private messages to self are sended just once
            if (data.senderUserName !== data.channel){
                socket.emit('user message', data);
            }
            var receiver = getSocketByUsername (connections, data.channel);
            if (receiver != null) {
                data.channel = socket.username;
                receiver.emit('user message', data);
                io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
            }
        }
    });

    socket.on('newUsernameRequest', function(data) {
        console.log('A username change request');
        var newName = data.message;
        if (!newName || typeof(newName)!=='string') {
            socket.emit('announcement', {message: 'There was an error'});
            return;
        }
        var re = new RegExp("[^a-zA-ZöäÖÄ0-9]");
        if (re.test(data.message)) {
            socket.emit('announcement', { message: 'Allowed characters: a-z, A-Z, öäÖÄ and 0-9' });
            return;
        }
        if (socket.username === newName) {
            socket.emit('announcement', { message: 'You are already called that!' });
            return;
        }
        else if (nameTaken(newName)){
            socket.emit('announcement', { message: 'username taken' });
            return;
        } else if (newName.length > 19) {
            socket.emit('announcement', { message: 'Please choose a username with less than 20 characters' });
            return;
        }
        socket.emit('username approved', { message: newName })
        var oldName = socket.username;
        socket.username = newName;
        console.log(oldName + ' is now known as ' + newName);
        io.sockets.emit('username change', { oldName: oldName, newName: newName });
        io.sockets.emit('announcement', {message: (oldName + ' is now known as ' + socket.username)});
        io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
    });

    socket.on('channel join request', function(data) {
        console.log('channel join request');
        if (data.channelName && typeof(data.channelName)==='string') {
            var channelName = data.channelName;
            var channel = getChannelByName(channels, channelName);
            if (channel != null) {
                if (channel.users.indexOf(socket) == -1) {
                    channel.users.push(socket);
                    console.log(socket.username + ' joined channel ' + channelName);
                }
                socket.emit('channel change accepted', {channelName: channelName});
                socket.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
            }
        }
    });

    socket.on('leave channel request', function(data) {
        console.log('room leave request: ' + data.channelName);
        if (data.channelName && typeof(data.channelName === 'string')) {
            var channel = getChannelByName(channels, data.channelName);
            if (channel != null) {
                tryRemoveUserFromChannel(channel, socket);
            }
            socket.emit('leaving channel approved', data);
            removeEmptyChannels(channels, 7);
            io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
        }
    });

    socket.on('new channel request', function(data) {
        if (channels.length > 15) {
            socket.emit('announcement', { message: 'There are too many channels!' })
            return;
        }
        if(data.message && typeof(data.message)==='string' && data.message.length < 20) {
            if(nameTaken(data.message)) {
                socket.emit('announcement', {message: 'sorry, that channel name is taken'});
                return;
            }
            var re = new RegExp("[^a-zA-ZöäÖÄ0-9]");
            if (re.test(data.message)) {
            socket.emit('announcement', { message: 'Allowed characters: a-z, A-Z, öäÖÄ and 0-9' });
            return;
        }
            var newChannel = data.message;
            channels.push({name: data.message, users: [socket]});
            socket.emit('channel change accepted', { channelName: newChannel });
            io.sockets.emit('update client', {userNames: usernameList(), channelNames: channelNames(channels)});
        }
    });

    socket.on('disconnect', function(data) {
        connections.splice(connections.indexOf(socket), 1);
        console.log('A socket disconnected, %s users in total', connections.length);
        removeUserFromChannels(channels, socket);
        removeEmptyChannels(channels, 7);
        io.sockets.emit('update client', { userNames: usernameList(), channelNames: channelNames(channels) });
    });
});

function giveDefaultUsername(socket) {
    var newUsername = 'newbie';
    while(nameTaken(newUsername)) {
        newUsername += Math.floor(Math.random() * 10);
    }
    socket.username = newUsername;
}

function nameTaken(name){
    for(i = 0; i < connections.length; i++) {
        if (connections[i].username === name) {
            return true;
        }
    }
    for (i = 0; i < channels.length; i++) {
        if (channels[i].name === name) {
            return true;
        }
    }
    return false;
}

function usernameList() {
    var list = [];
    for(i = 0; i < connections.length; i++) {
        var name = connections[i].username;
        if (name != null) {
            list.push(name);
        }        
    }
    return list;
}

function getSocketByUsername(connections, username) {
    for (i = 0; i < connections.length; i++) {
        if (connections[i].username === username) {
            return connections[i];
        }
    }
    return null;
}

function getChannelByName(channels, channelName) {
    if (channels && channelName){
        for(i = 0; i < channels.length; i++) {
            var channel = channels[i];
            if (channel.name === channelName) {
                return channel;
            }
        }
    }
    return null;
}

function channelNames(channels) {
    var names = [];
    for(i = 0; i < channels.length; i++) {
        names.push(channels[i].name);
    }
    return names;
}

function removeUserFromChannels(channels, socket) {
    for ( i = 0; i < channels.length; i++) {
        var channel = channels[i];
        tryRemoveUserFromChannel(channel, socket);
    }
}

function tryRemoveUserFromChannel(channel, socket) {
    var index = channel.users.indexOf(socket);
        if (index != -1) {
            channel.users.splice(index, 1);
            console.log(socket.username + ' left ' + channel.name);
        }
}

function removeEmptyChannels(channels, startingIndex) {
    var emptyChannels = [];
    for(i = startingIndex; i < channels.length; i++) {
        var channel = channels[i];
        if (channel.users.length == 0) {
            emptyChannels.push(channel);
        }
    }
    for (i = 0; i < emptyChannels.length; i++) {
        var channel = emptyChannels[i];
        channels.splice(channels.indexOf(channel, 1));
    }
}

console.log("Server is running at http://127.0.0.1:48915");
